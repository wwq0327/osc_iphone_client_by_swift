//
//  Person.swift
//  osc
//
//  Created by admin on 14/11/3.
//  Copyright (c) 2014年 sinosun. All rights reserved.
//

import Foundation
import CoreData

@objc(Person) class Person: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var password: String

}

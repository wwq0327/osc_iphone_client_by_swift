//
//  QuestionTableViewCell.swift
//  osc
//
//  Created by admin on 14/10/29.
//  Copyright (c) 2014年 sinosun. All rights reserved.
//

import UIKit

class QuestionTableViewCell: UITableViewCell {

    @IBOutlet var imageview: UIImageView!
    
    
    @IBOutlet var title: UILabel!
    
    
    @IBOutlet var subtitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

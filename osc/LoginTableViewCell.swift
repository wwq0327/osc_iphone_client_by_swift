//
//  LoginTableViewCell.swift
//  osc
//
//  Created by admin on 14/11/1.
//  Copyright (c) 2014年 sinosun. All rights reserved.
//

import UIKit

class LoginTableViewCell: UITableViewCell {

    @IBOutlet var label: UILabel!
    
    @IBOutlet var input: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

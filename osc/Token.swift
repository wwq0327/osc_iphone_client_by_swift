//
//  Token.swift
//  osc
//
//  Created by admin on 14/11/2.
//  Copyright (c) 2014年 sinosun. All rights reserved.
//

/*
{"uid":206314,"expires_in":262386,"token_type":"bearer","refresh_token":"6df25497-51ea-4a3e-bd23-d604f26845ca","access_token":"af98ff4d-3569-4dc7-9ef6-13a2dea90492"}
*/
import Foundation

class TokenM: JSONJoy {
    var access_token: String?
    var refresh_token: String?
    var token_type: String?
    var expires_in: Int?
    var uid: Int?

    
    required init(_ decoder: JSONDecoder) {
       self.access_token = decoder["access_token"].string
        self.refresh_token = decoder["refresh_token"].string
        self.token_type = decoder["token_type"].string
        self.expires_in = decoder["expires_in"].integer
        self.uid = decoder["uid"].integer
    }
    
}
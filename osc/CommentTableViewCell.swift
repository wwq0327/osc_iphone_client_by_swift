//
//  CommentTableViewCell.swift
//  osc
//
//  Created by admin on 14/11/14.
//  Copyright (c) 2014年 sinosun. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell, UIWebViewDelegate {

    @IBOutlet var myImageView: UIImageView!
    
    @IBOutlet var nameAndTime: UILabel!
    
    @IBOutlet var contentWebView: UIWebView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        // Initialization code
        self.contentWebView.scrollView.bounces = false
        self.contentWebView.scrollView.scrollEnabled = false
        // self.contentWebView.scalesPageToFit = true
        self.contentWebView.delegate = self
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    func webViewDidFinishLoad(webView: UIWebView){
        //在这里调整html
        self.contentWebView.stringByEvaluatingJavaScriptFromString("var tagHead =document.documentElement.firstChild;var tagStyle = document.createElement(\"style\");tagStyle.setAttribute(\"type\", \"text/css\");tagStyle.appendChild(document.createTextNode(\"BODY{padding: 0pt 0pt;width:220px;word-break:break-all;}\"));tagStyle.appendChild(document.createTextNode(\"div{width:220px }\"));var tagHeadAdd = tagHead.appendChild(tagStyle);")
        
        var x = self.contentWebView.frame.origin.x
        var y = self.contentWebView.frame.origin.y
        var w = self.contentWebView.frame.size.width
        var h = self.contentWebView.frame.size.height
        println("x=\(x) y=\(y) w=\(w) h=\(h)")

    }
    
}

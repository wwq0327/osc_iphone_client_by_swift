//
//  DtTableViewCell.swift
//  osc
//
//  Created by admin on 14/10/30.
//  Copyright (c) 2014年 sinosun. All rights reserved.
//

import UIKit

class DtTableViewCell: UITableViewCell {

    @IBOutlet var imageview: UIImageView!
    
    @IBOutlet var name: UILabel!
    
    @IBOutlet var title: UILabel!
    
    @IBOutlet var time: UILabel!
    
    @IBOutlet var icon: UIButton!
    
    @IBOutlet var messageNum: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

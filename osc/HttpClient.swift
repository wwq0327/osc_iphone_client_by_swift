//
//  HttpClient.swift
//  osc
//
//  Created by admin on 14/10/31.
//  Copyright (c) 2014年 sinosun. All rights reserved.
//

import Foundation
class HttpClient {
   class func loadData(url: String, parameters:[String:AnyObject], success: (()->Void), phaser:((data: NSData)->Void)){
        var request = HTTPTask()
        request.GET(url, parameters: parameters, success: { (response) -> Void in
            println("----------------------------------------------")
            println("response.statusCode=\(response.statusCode)")
            if response.responseObject != nil{
               // println("response.statusCode=\(response.statusCode)")
                var data = response.responseObject as NSData
                phaser(data: data)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    success()
                })
            }
            }) { (error) -> Void in
                println("-----------error=\(error)---------------------")
        }
    }
    
    class func putData(url: String, parameters:[String:AnyObject], success: (()->Void), phaser:((data: NSData)->Void)){
        var request = HTTPTask()
        request.POST(url, parameters: parameters, success: { (response) -> Void in
            if response.responseObject != nil{
                var data = response.responseObject as NSData
                phaser(data: data)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    success()
                })
            }
            }) { (error) -> Void in
                println("")
        }
    }
}

//
//  CommentM.swift
//  osc
//
//  Created by admin on 14/11/4.
//  Copyright (c) 2014年 sinosun. All rights reserved.
//
/*
{
"notice": {
"replyCount": 0,
"msgCount": 0,
"fansCount": 0,
"referCount": 0
},
"commentList": [
{
"content": "旋风网盘有,
<br>不好意思, 不混百度.",
"id": 283014280,
"pubDate": "2014-11-04 11:23:21",
"client_type": 0,
"commentPortrait": "http://static.oschina.net/uploads/user/499/998019_50.jpg?t=1364000678000",
"commentAuthor": "Tuesday",
"commentAuthorId": 998019,
"refers": [
{
"refertitle": "引用来自“xmut”的评论",
"referbody": "下载速度太慢了，建议放在百度等网盘上"
}
]
},
{
"content": "<div class='detail'>下载速度太慢了，建议放在百度等网盘上</div>",
"id": 283014197,
"pubDate": "2014-11-04 11:16:25",
"client_type": 0,
"commentPortrait": "http://static.oschina.net/uploads/user/48/96331_50.jpg",
"commentAuthor": "xmut",
"commentAuthorId": 96331,
"refers": [ ]
},
{
"content": "<div class='detail'>好  </div>",
"id": 283014146,
"pubDate": "2014-11-04 11:12:13",
"client_type": 0,
"commentPortrait": "http://static.oschina.net/uploads/user/1120/2241305_50.jpg?t=1412650032000",
"commentAuthor": "开源中国首席爱情专家",
"commentAuthorId": 2241305,
"refers": [ ]
},
{
"content": "意思是他是在xampp上面修改而来的？",
"id": 283013939,
"pubDate": "2014-11-04 10:54:55",
"client_type": 0,
"commentPortrait": "http://static.oschina.net/uploads/user/591/1182150_50.jpg?t=1387981308000",
"commentAuthor": "淫监会主席",
"commentAuthorId": 1182150,
"refers": [
{
"refertitle": "引用来自“Tuesday”的评论",
"referbody": "为什么这个软件取名Xampps，一看我还以为是Xampp。 就是为了混淆吗？"
},
{
"refertitle": "引用来自“淫监会主席”的评论",
"referbody": "饮水思源的意思,"
}
]
},
{
"content": "饮水思源的意思,",
"id": 283013906,
"pubDate": "2014-11-04 10:52:11",
"client_type": 0,
"commentPortrait": "http://static.oschina.net/uploads/user/499/998019_50.jpg?t=1364000678000",
"commentAuthor": "Tuesday",
"commentAuthorId": 998019,
"refers": [
{
"refertitle": "引用来自“淫监会主席”的评论",
"referbody": "为什么这个软件取名Xampps，一看我还以为是Xampp。 就是为了混淆吗？"
}
]
},
{
"content": "<div class='detail'>我去 这个还不错呢 版本越来越新了<br/></div>",
"id": 283013839,
"pubDate": "2014-11-04 10:46:35",
"client_type": 0,
"commentPortrait": "http://static.oschina.net/uploads/user/132/265297_50.jpg?t=1382610317000",
"commentAuthor": "理工小强",
"commentAuthorId": 265297,
"refers": [ ]
},
{
"content": "<div class='detail'>为什么这个软件取名Xampps，一看我还以为是Xampp。<br/>就是为了混淆吗？</div>",
"id": 283013816,
"pubDate": "2014-11-04 10:44:44",
"client_type": 0,
"commentPortrait": "http://static.oschina.net/uploads/user/591/1182150_50.jpg?t=1387981308000",
"commentAuthor": "淫监会主席",
"commentAuthorId": 1182150,
"refers": [ ]
},
{
"content": "<div class='detail'>这个是修改版的吧...发个官方的压缩绿色版http://sourceforge.net/projects/xampp/files/XAMPP%20Windows/1.8.3/xampp-portable-win32-1.8.3-5-VC11.7z/download</div>",
"id": 283013716,
"pubDate": "2014-11-04 10:36:20",
"client_type": 0,
"commentPortrait": "http://static.oschina.net/uploads/user/471/943633_50.jpg?t=1413778435000",
"commentAuthor": "绿薯",
"commentAuthorId": 943633,
"refers": [ ]
}
]
}

*/
import UIKit


class CommentList: JSONJoy {
    var notice: NoticeM
    var commentList: [CommentDetailM]
    
    init(){
        self.notice = NoticeM()
        self.commentList = Array<CommentDetailM>()
    }
    required init(_ decoder: JSONDecoder) {
        commentList = Array<CommentDetailM>()
        if let comments = decoder["commentList"].array {
            for comment: JSONDecoder in comments {
                commentList.append(CommentDetailM(comment))
            }
        }
        self.notice = NoticeM(decoder["notice"])
    }
    
    
    var count: Int{
        return self.commentList.count
    }
    
    
    func append(list: [CommentDetailM]){
        self.commentList = list + self.commentList
    }
    
    
}



class CommentDetailM: JSONJoy {
    var content: String?
    var id: Int?
    var pubDate: String?
    var client_type: String?
    var commentPortrait: String?
    var commentAuthor: String?
    var commentAuthorId: Int?
    var refers: [Refer]?
    
    required init(_ decoder: JSONDecoder) {
        self.content = decoder["content"].string
        self.id = decoder["id"].integer
        self.pubDate = decoder["pubDate"].string
        self.client_type = decoder["client_type"].string
        self.commentPortrait = decoder["commentPortrait"].string
        self.commentAuthor = decoder["commentAuthor"].string
        self.commentAuthorId = decoder["commentAuthorId"].integer
        
        refers = Array<Refer>()
        for refer:JSONDecoder in decoder["refers"].array!{
            refers?.append(Refer(refer))
        }
        
    }
}


class Refer: JSONJoy {
    var refertitle: String?
    var referbody: String?
    
    required init(_ decoder: JSONDecoder) {
        self.refertitle = decoder["refertitle"].string
        self.referbody = decoder["referbody"].string
    }
}


class CommentDetailMV: NSObject {
    var id : Int?
    var portrait_image_url: String?
    var nameAndTime: String?
    var content: String?
    var height: CGFloat?
    
    init(commentDetailM: CommentDetailM){
        super.init()
        self.id = commentDetailM.id
        self.content = commentDetailM.content
        
        self.portrait_image_url = commentDetailM.commentPortrait
        self.nameAndTime = String(format: "%@ 发布于%@", commentDetailM.commentAuthor!, commentDetailM.pubDate!)
        
        var font = UIFont(name: "arial", size: 14.0)
        self.height = self.getTextViewHeight(font!, text: content!)
    
    }
    
    
    func getTextViewHeight(font: UIFont, text: NSString)->CGFloat{
        var rect = text.boundingRectWithSize(CGSizeMake(295, CGFloat.max), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName : font], context: nil)
        return rect.size.height
    }
}


























//
//  News.swift
//  osc
//
//  Created by admin on 14/10/30.
//  Copyright (c) 2014年 sinosun. All rights reserved.
//

/*
{
"newslist": [
    {
    "id": 56593,
    "author": "oschina",
    "pubDate": "2014-10-30 15:06:29",
    "title": "Netty 4.0.24.Final 发布",
    "authorid": 1,
    "commentCount": 3,
    "type": 4
    },
    {
    "id": 56592,
    "author": "oschina",
    "pubDate": "2014-10-30 14:46:36",
    "title": "foobar2000 1.3.5 正式版发布",
    "authorid": 1,
    "commentCount": 11,
    "type": 4
    },
    {
    "id": 56591,
    "author": "MarChen",
    "pubDate": "2014-10-30 13:45:17",
    "title": "UltimateAndroid 0.2.0 发布，Material Design效果",
    "authorid": 988131,
    "commentCount": 1,
    "type": 4
    }
],
"notice": {
    "replyCount": 0,
    "msgCount": 0,
    "fansCount": 0,
    "referCount": 0
    }
}
*/
import Foundation

/*
*/

// MARK: -NewsList
class NewsList: JSONJoy {
    var newslist: [NewsM]
    var notice: NoticeM
    
    var count: Int{
        return self.newslist.count
    }
    
    init(){
        newslist = Array<NewsM>()
        notice = NoticeM()
    }
    required init(_ decoder: JSONDecoder) {
        newslist = Array<NewsM>()
        for news: JSONDecoder in decoder["newslist"].array!{
            newslist.append(NewsM(news))
        }
        notice = NoticeM(decoder["notice"])
    }
    
    func append(list: [NewsM]){
        self.newslist = list + self.newslist
    }

}

// MARK: -NewsM
class NewsM: JSONJoy{
    var id: Int?
    var author: String?
    var pubDate: String?
    var title: String?
    var authorid: Int?
    var commentCount: Int?
    var type: Int?
    
    init() {
        
    }
    
    required init(_ decoder: JSONDecoder){
        id = decoder["id"].integer
        author = decoder["author"].string
        pubDate = decoder["pubDate"].string
        title = decoder["title"].string
        authorid = decoder["authorid"].integer
        commentCount = decoder["commentCount"].integer
        type = decoder["type"].integer
    }
}

// MARK: -NewsMV
class NewsMV {
    var id : Int?
    var news_title: String?
    var sub_title: String?
    var authorid: Int?
    var type: Int?
    init(){
        
    }
    
    init(news: NewsM){
        self.id = news.id
        self.news_title = news.title
        //作者 时间 （5评）
        self.sub_title = String(format: "%@%@(%i评)", news.author!, news.pubDate!, news.commentCount!)
        self.authorid = news.authorid
        self.type = news.type
    }
}

/*

{
"id": 56621,
"body": "<style type='text/css'>pre {white-space:pre-wrap;word-wrap:break-word;}</style><p> OpenVPN 2.3.5发布。2014-10-29 因为众所周知的原因，OpenVPN主站(openvpn.net)被墙了.上一个版本是2014-05-02的2.3.4 .此版本主要修正了和windows tap-windows6驱动不兼容的问题及其他的一些小Bug修正和增强。遗留产品线2.1.4/2.2.2。</p>
<p></p>
<p> OpenVPN 是一个基于 OpenSSL 库的应用层 VPN 实现。和传统 VPN 相比，它的优点是简单易用。 </p>
<p><span style="color:#333399;">OpenVPN 允许参与建立VPN的单点使用共享金钥，电子证书，或者用户名/密码来进行身份验证。它大量使用了OpenSSL加密库中的SSLv3/TLSv1  协议函式库。目前OpenVPN能在Solaris、Linux、OpenBSD、FreeBSD、NetBSD、Mac OS X与Windows  2000/XP/Vista上运行，并包含了许多安全性的功能。它并不是一个基于Web的VPN软件，也不与IPsec及其他VPN软件包兼容。</span></p>
<p><span style="color:#333399;">OpenVPN使用OpenSSL库加密数据与控制信息：它使用了OpenSSL的加密以及验证功能，意味着，它能够使用任何OpenSSL支持的算法。它提供了可选的数据包HMAC功能以提高连接的安全性。此外，OpenSSL的硬件加速也能提高它的性能。</span></p>
<p></p>
<p> 完全改进： </p>
<h2> OpenVPN 2.3.5 -- released on 2014.10.28 (<a href="https://108.61.220.71/browse.php?u=3lJFFlLLnXKGWiVBDcl%2BKmRh9jsTn1sgeHQKsTBi4izgWVLha4I12z%2FKTyDqezn7EF2MLvf1MI8o&b=14" rel="nofollow">Change Log</a>) </h2>
<p> This release fixes a serious interoperability issue with OpenVPN and the <a href="https://108.61.220.71/browse.php?u=3lJFFlbNhHeGVmJWG4o%2BFXFq7h0t%2FxoxbStItjxi8DP5BRM%3D&b=14" rel="nofollow">tap-windows6 driver</a>. In addition a fair number of other bug fixes and small enhancements are included. </p>
<h2> OpenVPN 2.3.5 </h2>
<pre class="wiki">Andris Kalnozols (2):
Fix some typos in the man page.
Do not upcase x509-username-field for mixed-case arguments.

Arne Schwabe (1):
Fix server routes not working in topology subnet with --server [v3]

David Sommerseth (4):
Improve error reporting on file access to --client-config-dir and --ccd-exclusive
Don't let openvpn_popen() keep zombies around
Add systemd unit file for OpenVPN
systemd: Use systemd functions to consider systemd availability

Gert Doering (4):
Drop incoming fe80:: packets silently now.
Fix t_lpback.sh platform-dependent failures
Call init script helpers with explicit path (./)
Preparing for release v2.3.5 (ChangeLog, version.m4)

Heiko Hund (1):
refine assertion to allow other modes than CBC

Hubert Kario (2):
ocsp_check - signature verification and cert staus results are separate
ocsp_check - double check if ocsp didn't report any errors in execution

James Bekkema (1):
Fix socket-flag/TCP_NODELAY on Mac OS X

James Yonan (6):
Fixed several instances of declarations after statements.
In socket.c, fixed issue where uninitialized value (err) is being passed to to gai_strerror.
Explicitly cast the third parameter of setsockopt to const void * to avoid warning.
MSVC 2008 doesn't support dimensioning an array with a const var nor using %z as a printf format specifier.
Define PATH_SEPARATOR for MSVC builds.
Fixed some compile issues with show_library_versions()

Jann Horn (1):
Remove quadratic complexity from openvpn_base64_decode()

Mike Gilbert (1):
Add configure check for the path to systemd-ask-password

Philipp Hagemeister (2):
Add topology in sample server configuration file
Implement on-link route adding for iproute2

Samuel Thibault (1):
Ensure that client-connect files are always deleted

Steffan Karger (13):
Remove function without effect (cipher_ok() always returned true).
Remove unneeded wrapper functions in crypto_openssl.c
Fix bug that incorrectly refuses oid representation eku's in polar builds
Update README.polarssl
Rename ALLOW_NON_CBC_CIPHERS to ENABLE_OFB_CFB_MODE, and add to configure.
Add proper check for crypto modes (CBC or OFB/CFB)
Improve --show-ciphers to show if a cipher can be used in static key mode
Extend t_lpback tests to test all ciphers reported by --show-ciphers
Don't exit daemon if opening or parsing the CRL fails.
Fix typo in cipher_kt_mode_{cbc, ofb_cfb}() doxygen.
Fix regression with password protected private keys (polarssl)
ssl_polarssl.c: fix includes and make casts explicit
Remove unused variables from ssl_verify_openssl.c extract_x509_extension()

TDivine (1):
Fix "code=995" bug with windows NDIS6 tap driver.</pre>
<p>下载 http://fossies.org/linux/misc/openvpn-2.3.5.tar.gz</p>
<p></p>",
"pubDate": "2014-10-31 09:48:43",
"author": "fei",
"title": "OpenVPN 2.3.5 发布",
"authorid": 3138,
"relativies": [
{
"title": "Zabbix 2.3.5 (beta) 发布，分布式系统监视",
"url": "http://www.oschina.net/news/55003/zabbix-2-3-5-beta"
},
{
"title": "Elive 2.3.5 (Beta) 发布，Linux 发行版",
"url": "http://www.oschina.net/news/54503/elive-2-3-5-beta"
},
{
"title": "OpenVPN 2.3.4 发布",
"url": "http://www.oschina.net/news/53100/openvpn-2-3-4"
},
{
"title": "Ejscript 2.3.5 发布，服务端的 JavaScript",
"url": "http://www.oschina.net/news/51865/ejscript-2-3-5"
},
{
"title": "HeartBleed 漏洞会暴露 OpenVPN 私钥",
"url": "http://www.oschina.net/news/51010/heartbleed-exposed-openvpn-private-key"
},
{
"title": "OpenVPN 2.3.3 发布",
"url": "http://www.oschina.net/news/50721/openvpn-2-3-3"
},
{
"title": "Grails 2.3.5 GA 发布",
"url": "http://www.oschina.net/news/48905/grails-2-3-5-ga"
},
{
"title": "DBeaver 2.3.5 发布，数据库管理工具",
"url": "http://www.oschina.net/news/47200/dbeaver-2-3-5"
},
{
"title": "OpenVPN 2.3.2 发布",
"url": "http://www.oschina.net/news/41929/openvpn-2-3-2"
},
{
"title": " OpenVPN 2.3.1 发布",
"url": "http://www.oschina.net/news/39367/openvpn-231"
}
],
"notice": {
"replyCount": 0,
"msgCount": 0,
"fansCount": 0,
"referCount": 0
},
"favorite": 0,
"commentCount": 4,
"url": "http://www.oschina.net/news/56621/openvpn-2-3-5"
}

*/

// MARK: -NewsDetailM
class NewsDetailM: JSONJoy {
    var id: Int?
    var body: String?
    var pubDate: String?
    var author: String?
    var title: String?
    var authorid: Int?
    var relativies: [RelativesNewsM]?
    var notice: NoticeM?
    var favourite: Int?    //是否收藏 1-收藏 0-未收藏
    var commentCount: Int?
    var url: String?
    
    required init(_ decoder: JSONDecoder) {
        self.id = decoder["id"].integer
        self.body = decoder["body"].string
        self.pubDate = decoder["pubDate"].string
        self.author = decoder["author"].string
        self.title = decoder["title"].string
        self.authorid = decoder["authorid"].integer
        
        relativies = Array<RelativesNewsM>()

        for relativie: JSONDecoder in decoder["relativies"].array!{
            relativies?.append(RelativesNewsM(relativie))
        }
        
        self.notice = NoticeM(decoder["notice"])
        self.favourite = decoder["favorite"].integer
        self.commentCount = decoder["commentCount"].integer
        self.url = decoder["url"].string
    }
}

class NewsDetailMV {
    var id: Int?
    var title: String?
    var subTitle: String?
    var body: String?
    var relativies: [RelativesNewsM]?
    var url: String?
    var favourite: Int?
    
    init(newDetail: NewsDetailM){
        self.id = newDetail.id
        self.title = newDetail.title
        self.body = newDetail.body
        self.subTitle = String(format: "%@ 发布于%@", newDetail.title!, newDetail.pubDate!)
        self.relativies = newDetail.relativies
        self.url = newDetail.url
        self.favourite = newDetail.favourite
    }
    
    func generateRelativeNewsString(relativeNews: Array<RelativesNewsM>)->String{
        var result: String = ""
        if relativeNews == NSNull() || relativeNews.count == 0 {
            return ""
        }
        
        for relative in relativeNews {
            result = String(format: "%@<a href=%@ style='text-decoration:none'>%@</a><p/>",result, relative.url!, relative.title!  )
        }
        return String(format: "<hr/>相关文章<div style='font-size:14px'><p/>%@</div>", result)
    }
    
    func toHtml(newsDetailMV: NewsDetailMV)->String{
        var html = ""
        var software = ""
        var relativeNews = generateRelativeNewsString(newsDetailMV.relativies!)
        html = String(format: "<body style='background-color:#EBEBF3'>%@<div id='oschina_title'>%@</div><div id='oschina_outline'>%@</div><hr/><div id='oschina_body'>%@</div>%@%@%@</body>", HTML_Style, newsDetailMV.title!, newsDetailMV.subTitle!, newsDetailMV.body!, software,relativeNews, HTML_Bottom )
        
        return html
    }
}

// MARK: -RelativesNewM
class RelativesNewsM:JSONJoy {
    var title: String?
    var url: String?
    
    required init(_ decoder: JSONDecoder) {
        self.title = decoder["title"].string
        self.url = decoder["url"].string
    }
}



// MARK: -NoticeM
class NoticeM: JSONJoy {
    var replyCount: Int?
    var msgCount: Int?
    var fansCount: Int?
    var referCount: Int?
    
    init(){}
    required init(_ decoder: JSONDecoder) {
        self.replyCount = decoder["replyCount"].integer
        self.msgCount = decoder["msgCount"].integer
        self.fansCount = decoder["fansCount"].integer
        self.referCount = decoder["referCount"].integer
    }
}





//MARK: -FavouriteOperation
class FavouriteOperation: JSONJoy {
    var error: String?
    var error_description: String?
    
    required init(_ decoder: JSONDecoder) {
        self.error = decoder["error"].string
        self.error_description = decoder["error_description"].string
    }
}
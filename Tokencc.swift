//
//  Tokencc.swift
//  osc
//
//  Created by admin on 14/11/3.
//  Copyright (c) 2014年 sinosun. All rights reserved.
//

import Foundation
import CoreData

@objc(Tokencc) class Tokencc: NSManagedObject {

    @NSManaged var access_token: String
    @NSManaged var refresh_token: String
    @NSManaged var token_type: String
    @NSManaged var expires_in: NSNumber
    @NSManaged var uid: NSNumber
    @NSManaged var is_online: NSNumber
    @NSManaged var date_created: NSDate

}
